//Total load balance 
let loadBalance = document.querySelector("#balance")
loadBalance.innerText = 500

//Mobile Number
let number = document.querySelector('#number')

// History 
let history = document.querySelector('#history')
history.innerHTML = ''
//Loading Process 
function loadingProcess(amount) {

    //number checker 
    if (number.value.length < 11) { // number is from the id we gave
        return alert ('Enter a Number')
    } 

    //load balance checker
    if(loadBalance.innerText < amount) {
        return alert ('Insufficient Balance')
    }
    loadBalance.innerText -= amount 
    history.innerHTML += `₱ ${amount} is loaded to ${number.value} \n`

}


let parentButton = document.querySelector('.parentButton')
parentButton.addEventListener('click', (e) => {
    let buttonValue = e.target.innerText.slice(2);


    loadingProcess (parseInt(buttonValue))
})



